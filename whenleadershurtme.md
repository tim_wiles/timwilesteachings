What do I do when my leaders hurt me?
=============================================

# Introduction #

You can either do the bible study on your own or with a friend over coffee. (Doing bible studies with others can be great as you get other people's stories and experiences too, but if there's no-one around I just write my answers down to God.)

Try to avoid dwelling on any one question. Just breeze through them putting down (or saying out loud) the first thing that comes into your head. It's so easy to think too hard about these things and try to use our brains rather than letting God speak to our spirits. And remember the aim of the study is to find out what the word of God says, not to share our opinions.

We'll look at the the books of 1 Peter, Acts, and 1 John  (letters by some of Jesus's friends), Psalm 131, Romans, and some of Jesus's words in the gospel of Luke.

And do print out and mark the text if you can. It really helps focus.

# What do I do when my leaders hurt me? #


In this series of 40 minute bible studies we'll look at what to do when our leaders behave in a way that isn't Christ-like. When they rebuke us for things
we haven't done wrong. When they ignore wrong and injustice. When they are hypocritical or when they teach things that are contrary to the things
our Lord taught us when he spoke to his disciples 2000 years ago....

...When he spoke with words so powerful and comforting that they somehow manage to touch the hearts of people 2000 years later in a completely
different culture. And make them feel like they are hearing from their long-lost father.

Intrigued? Keep reading.


# Session 1 #

Let's first look at who God is. 


## Read 1 John 4:15-16, 4:18 ##

https://www.biblegateway.com/passage/?search=1+john+4%3A15-16%2C18&version=NLT

As you read, mark the word God (and pronouns like He and His) with a triangle. Mark the word
love with a heart.

* What do you learn by marking the words God and Love? (really simple answers)

* According to the text, who has God living inside them?

The word "live" in verse 16 is actually "abide" (menōn in Greek). It means to have somewhere as your dwelling place.
The place you always come back to. (even if we sometimes stray away)

Now read the verses again. This time, mark the words fear and afraid with a wavy line. Mark the word trust with a tick.

* What do you learn from marking the references to fear and trust? (really simple answers)

* What is the antidote to fear, according to this passage?

* What things make you afraid? These can be general things or specific things?
  What are your worst nightmares?

* What things can our leaders do to make us afraid?

* Can you think of a time in your life when you acted out of fear rather than love? How might
  understanding God's love for you have changed the way you acted or thought? Be specific.

Now let's look at how to know whether our leaders are worth trusting or not.

## Now read 1 John 4:1-3 and Romans 13:1 ##

As you read, mark the word Jesus with a cross. Mark the word Know with a K. Mark the word God with a triangle.

**Insight: John wrote the letter of 1 John to warn some other believers about false teachers,
many of whom were saying that believing in Jesus's physical existence and resurrection wasn't necessary.
(read 1 John 2:26)**

* How can we discern if a teacher / leader has the Spirit of Christ?

* What do you learn about God's plan from these verses?

* Does this necessarily mean everything a teacher / leader says is true, or that everything 
  that a leader does is what God would do? Explain your answer. (We'll come back to this question later)

## Now read Luke 2:51 and Romans 14:1 ##

**Insight: In Luke chapter 2, Jesus has just spent some time in the temple among the religious leaders
 proving **

As you read, mark the word accept with a smiley face, and obey with a tick.

* What do you learn from marking the words "accept" and "obey"?

* Can you imagine a situation where it may have been difficult for Jesus to obey his parents?


# Session 2 #

Last time we saw some basic principles for knowing how and when to trust our spiritual leaders 
and our parents.

In this session, we'll look at governing authorities and our bosses at work. 

We'll see that our leaders sometimes make decisions we don't understand or agree with.
This could be due to

1.   them knowing something we don't know,

2.   us knowing something they don't know,

3.   them having impure motives.

In each case, we'll see that *obeying* such leaders can deepen our relationship with God and our understanding of others...
and can also allow our leaders themselves to grow.

Let's have a look at some occasions when people in the bible chose to obey / disobey authority.


## Read Acts 4:18-22, Rom 12:2 ##

As you read, mark references to "leader", "government" or some other earthly authority with a double underline.
Mark references to God with a triangle. Mark the references to the world with a capital W.

*   What do you learn from marking the references to authorities? How would you describe the religious
    leaders in Jesus's time?

In Acts 4, we saw the religious leaders giving a direct command to the apostles, which the
apostles disobeyed. 

*   What reason did the apostles give for disobeying the direct command of the authorities?

*   Can you think of a time in your life when God has given you an explicit command to do
    or not do something? Or when you have known
    that something was wrong / right from the Holy Scriptures?

*   What particular beliefs and customs do you see in the world around you that God wouldn't approve of?
    Which of these things do people do because they have been 
    told to by someone in authority?

*   Do you feel comfortable knowing when to disobey corrupt authorities and social norms even if it means suffering
    yourself?

Let's now look look at times when people in the bible obeyed authority or submitted to their neighbours.

## Have a read of Acts 23:2-5 ##

Again, mark references to earthly authorities with a double underline.
Mark references to God with a triangle. Mark the word "Sorry" with a capital "S".

**Insight: In this story, Paul has made some Jewish religious people angry with his preaching of
the truth, and has
been dragged before the high council. He is all alone. But God is with him**

*   How does Paul's reaction reflect his beliefs about authority (think back to what he 
    wrote in Rom 13v1 which we studied in the last session).

*   What gives Paul his confidence and his peacefulness even when he is being attacked and humiliated?

*   How does the way we react to criticism reveal the state of our heart? How can we be more like Paul?

Paul stood out from the crowd not because of his intellect or clever-ness (although he
knew how to use his intellect when the time was right). But because of his simple, consistent preaching of 
the message of the cross... and his honest, gentle character (And the signs and wonders the Holy Spirit enabled
him to perform, of course).

These things made him very dangerous to the religious establishment at the time.

In Luke 6, we learn about how to treat our neighbours and enemies. Especially those who believe they
have a right to something that we currently have.

## Read Luke 6:29-30 and Luke 20:20-25 ##

As you read, mark references to giving with a present symbol. Mark references to God with a triangle as before.

**Insight: In another story, Jesus shows Peter how God can supernaturally provide for us
when we have taxes to pay, so as not to cause offence to others. See Matthew 17:27**

*   In what way does submitting to others make us more like Jesus?

*   What belongs to God?

*   When we obey authority, and others see us doing so, what message does it send to them?

*   Which do you think is more valuable to God? Why?

    1. Us learning to be obedient.

    2. Our reputation.

    2. Physical money, success, productivity.

We've seen that obeying our leaders can help us to learn how to obey God. Even if sometimes our 
leaders will do things that hurt us either deliberately or unintentionally. But God will always
seek our long-term good, as that is what He has promised to anyone who loves Him and accepts His son.

As we conclude this session, let's have a look at one other advantage to obedience: peace.

## Read Psalm 131v1-3 ## 

As you read, mark the words relating to David (the author of the psalm) with a crown (me, my, etc.).
Mark words relating to God with a triangle. Mark the word child with a picture of a baby.

*   What do you learn about David's attitude to God?

*   What do you learn about David's attitude to himself?

*   How does David feel in his soul when he puts his trust in God? What analogy does he use?

*   Why do you think that David uses this analogy?

*   How (practically) might a person put his/her trust in the Lord?

*   How do you feel about applying this in your life when faced with a difficult situation?

## Conclusion to session 2 ##

In this session, we saw that oftentimes leaders and authorities are corrupt and greedy.
We saw that they will often make demands of us (such as taxes) that are unreasonable.

We saw that sometimes they tell us to do things that are a direct contradiction to what
God has told us to do. And sometimes their instructions don't contradict a commandment
but nevertheless seem like a bad idea to us, or we know they are motivated by evil desires.

This is because our leaders are sinful humans like us. Thank God for Jesus who cleanses
us from all the hidden sins in our hearts and declares us righteous before God!

Knowing when and how to obey our leaders and submit to our neighbours can be very difficult. 
Especially if we are so used to doing things on our own (as I am).

But the rewards to cooperation are great:
We can learn more about God, lead lives of peace, and lighten our burdens (since we have fewer
decisions to make). Even if our obeying others will sometimes lead to less productivity 
/ success / money  in the short term.

*   So what then would be a sensible prayer for our leaders? Can you think of one?

In session 3, we'll look the book of 1 Peter. And how we can apply all we've learned to
God's church.